<?php

namespace Foods;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'foods' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/foods[/:action][/:id]',
                    'defaults' => [
                        'controller' => Controller\FoodsController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'paginator'=>[
                'type'=>Segment::class,
                'options'=>[
                    'route'=>'/paginator[/:action[/page[/:page]]]',
                    'defaults'=>[
                        'controller'=>Controller\PaginatorController::Class,
                        'action'=>'index',
                        'page'=>1
                    ]
                ],
                'constraints'=>[
                    'action'=>'[a-zA-Z0-9_-]*',
                    'page'=>'[0-9]*'
                ]
            ]
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\PaginatorController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            'foods' => __DIR__ . '/../view',
        ],
    ],
];
