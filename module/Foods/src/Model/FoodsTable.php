<?php

namespace Foods\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Sql;

class FoodsTable{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //select
    public function fetchAll()
    {
        //return $this->tableGateway->select();
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from(['f'=>'foods'])
        ->columns(['idMonan'=>'id','tenMonan'=>'name','summary','price','promotion','image'])
        ->join(['t'=>'food_type'],'t.id = f.id_type',['name_type'=>'name'])
        ->order('name_type');

        $selectString = $sql->buildSqlString($select);
        $rs = $adapter->query($selectString,$adapter::QUERY_MODE_EXECUTE);

        return $rs;
    }
    //getTable
    public function getTableName(){
        return $this->tableGateway->getTable();
    }
    public function selectData(){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('foods')->where('id = 2');

        $statement = $sql->prepareStatementForSqlObject($select);
        $rs = $statement->execute();

        return $rs;
    }

    public function selectData02(){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from(['f'=>'foods'])
        ->columns(['mamon'=>'id','tenmon'=>'name','dongia'=>'price'])
        ->join(['t'=>'food_type'],'f.id_type = t.id',[],$select::JOIN_LEFT)
        ->where(['f.id'=>2]);

        $statement = $sql->prepareStatementForSqlObject($select);
        $rs = $statement->execute();

        return $rs;
    }
    
    public function getFoodType(){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from(['t'=>'food_type'])
        ->columns(['id','name']);

        $selectString = $sql->buildSqlString($select);
        $rs = $adapter->query($selectString,$adapter::QUERY_MODE_EXECUTE);

        return $rs;
    }

    public function saveFoods(Foods $foods){
        $data = [
             'id_type'=>$foods->id_type,
             'name'=>$foods->name,
             'summary'=>$foods->summary,
             'detail'=>$foods->detail,
             'price'=>$foods->price,
             'promotion'=>$foods->promotion,
             'image'=>$foods->image,
             'update_at'=>$foods->update_at,
             'unit'=>$foods->unit,
             'today'=>$foods->today
        ];
        $id = (int)$foods->id;
        if($id<=0){
            $this->tableGateway->insert($data);
        }else if(!$this->findFoods($id)){
            throw new RuntimeException("Updat không thành công. Không tìm thấy món ăn có id là: $id");
        }
        else{
            $this->tableGateway->update($data,['id'=>$id]);
        }
        return;
    }

    public function findFoods($id){
        $id = (int)$id;
        $foods = $this->tableGateway->select(['id'=>$id]);
        $foods = $foods->current(); // tra ve một mảng duy nhất
        if(!$foods){
            throw new RuntimeException("Không tìm thấy món ăn có id là: $id");
        }
        return $foods;
    }
    function deleteFood($id){
        $this->tableGateway->delete(['id'=>$id]);
    }

}