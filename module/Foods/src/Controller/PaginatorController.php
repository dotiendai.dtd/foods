<?php
namespace Foods\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter;
use Zend\Db\Adapter\Adapter as ADB;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Db\Sql\Select;

class PaginatorController extends AbstractActionController{
    function indexAction(){
        $arrayData = [
            [
                'name'=>'Sản phẩm 1',
                'price'=>200000
            ],
            [
                'name'=>'Sản phẩm 2',
                'price'=>300000
            ],

            [
                'name'=>'Sản phẩm 3',
                'price'=>500000
            ],
            [
                'name'=>'Sản phẩm 4',
                'price'=>700000
            ],
            [
                'name'=>'Sản phẩm 5',
                'price'=>4300000
            ],
            [
                'name'=>'Sản phẩm 6',
                'price'=>3100000
            ],

        ];
        $paginator = new Paginator(new Adapter\ArrayAdapter($arrayData));
        $currentPage = $this->params()->fromRoute('page',1);
        $paginator->setCurrentPageNumber($currentPage);
        $paginator->setItemCountPerPage(2);
        $paginator->setPageRange(5);
        $vm = new ViewModel();
        $vm->setVariable('paginator', $paginator);
        return $vm;

    }
    public function AdapterDB(){
        $adapter = new ADB([
            'driver'=>'Pdo_Mysql',
            'database'=>'ql_nhahang',
            'username'=>'root',
            'password'=>'',
            'hostname'=>'localhost',
            'charset'=>'utf8'
        ]);
            return $adapter;
    }
    public function index02Action(){
        $adapter = $this->AdapterDB();
        $select = new Select();
        $select->from('foods');

        $dbselect = new DbSelect($select,$adapter);
        $paginator = new Paginator($dbselect);
        $currentPage = $this->params()->fromRoute('page',1);
        $paginator->setCurrentPageNumber($currentPage);
        $paginator->setItemCountPerPage(5);
        $paginator->setPageRange(5);
        $vm = new ViewModel();
        $vm->setVariable('paginator', $paginator);
        return $vm;
    }
}