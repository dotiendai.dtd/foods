<?php

namespace Foods\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Foods\Model\FoodsTable;
use Foods\Form\FoodsForm;
use Zend\File;
use Zend\Filter\File\Rename;
use Foods\Model\Foods;
use Zend\Validator\ValidatorChain;
use Zend\Validator\File\Size;
use Zend\Validator\File\MimeType;

class FoodsController extends AbstractActionController{

    // Add this property:
    private $table;

    // Add this constructor:
    public function __construct(FoodsTable $table)
    {
        $this->table = $table;
    }

    public function indexAction()
    {
        $foods = $this->table->fetchAll();
        // foreach($foods as $f){
        //    echo "<pre>";
        //     print_r($f);
        //     echo "</pre>";
        // }
        // return false;
        return new ViewModel(['foods'=>$foods]);
    }

    public function getTableNameAction(){
        $name = $this->table->getTableName();
        echo $name;
        return false;
    }

    public function selectDataAction(){
        $data = $this->table->selectData();
        foreach($data as $row){
            echo "<pre>";
            print_r($row);
            echo "</pre>";
        }

        return false;
    }
    public function selectData02Action(){
        $data = $this->table->selectData02();
        foreach($data as $row){
            echo "<pre>";
            print_r($row);
            echo "</pre>";
        }

        return false;
    }

    public function addAction()
    {
        $form = new FoodsForm('add');
       // print_r($form); die;
        $typeFoods = $this->table->getFoodType();
        // foreach($typeFoods as $t){
        //     print_r($t);
        // }
        // return false;
        $listType = [];
        foreach($typeFoods as $type){
            $listType[$type->id] = $type->name;
        }
      //  print_r($listType); die;
        $form->get('id_type')->setValueOptions($listType);
        $checkRequest = $this->getRequest();
        if(!$checkRequest->isPost()){
            return new ViewModel(['form'=>$form]);
        }

        $data = $checkRequest->getPost()->toArray();
        $file = $checkRequest->getFiles()->toArray();
        $data = array_merge_recursive($data, $file);

        //print_r($data); die;
        $form->setData($data);
     
        if(!$form->isValid()){
            $this->flashMessenger()->addErrorMessage('Thêm không thành công, vui lòng kiểm tra lại!');
            return new ViewModel(['form'=>$form]);
        }
        $data = $form->getData();
        
        $newName = date('Y-m-d-h-i-s').'-'.$file['image']['name'];

        $image = new Rename([
            'target'=>IMAGE_PATH.'/hinh_mon_an/'.$newName,
            'overwrite'=>true
        ]);

        $image->filter($file['image']);
        $data['update_at'] = date('Y-m-d');
        $data['image'] = $newName;
       //print_r($data); die;
        //$data['promotion'] = implode($data['promotion'],', '); //Đưa giá trị dạng mảng thành dạng chuỗi
        $foods = new Foods;
        $foods->exchangeArray($data);
        $this->table->saveFoods($foods);
        $this->flashMessenger()->addSuccessMessage('Thêm thành công!');
        return $this->redirect()->toRoute('foods',['controller'=>'FoodsController','action'=>'index']);
    }

    public function editAction()
    {
        $id = (int)$this->params()->fromRoute('id',0); //default 0
        if($id == 0){
            return $this->redirect()->toRoute('foods',['controller'=>'FoodsController','action'=>'index']);
        }
        $food = $this->table->findFoods($id);
        
        // echo "<pre>";
        // print_r($food);
        // echo "</pre>";
        // return false;
        $form = new FoodsForm('edit');
        $form->bind($food); // Đưa data select ra được vào input tương ứng
        $typeFoods = $this->table->getFoodType();
        // foreach($typeFoods as $t){
        //     print_r($t);
        // }
        // return false;
        $listType = [];
        foreach($typeFoods as $type){
            $listType[$type->id] = $type->name;
        }
      //  print_r($listType); die;
        $form->get('id_type')->setValueOptions($listType);

        $checkRequest = $this->getRequest();
        if(!$checkRequest->isPost()){
            return new ViewModel(['form'=>$form]);
        }
        $data = $checkRequest->getPost()->toArray();
        $file = $checkRequest->getFiles()->toArray();
       
        if($file['image']['error'] <= 0){
           
            $validatorChain = new ValidatorChain();
            $size = new Size(['min'=>20*1024,'max'=>2*1024*1024]);
            $size->setMessages([
                Size::TOO_SMALL => 'File quá nhỏ, ít nhất %min% kb',
                Size::TOO_BIG=>'Dung lượng quá lớn, tối đa %max% kb'
            ]);
            $mimeType = new MimeType('image/jpg, image/png, image/jpeg');
            $mimeType->setMessages([
                MimeType::FALSE_TYPE=>'Kiểu file %type% không được phép chọn',
                MimeType::NOT_DETECTED=>'Không xác định',
                MimeType::NOT_READABLE=>'Không thể đọc file'
            ]);
            $validatorChain->attach($mimeType,true,2)->attach($size,true,1);
            if(!$validatorChain->isValid($file['image'])){
                $message = $validatorChain->getMessages();
                $form->get('image')->setMessages($message);
                return new ViewModel(['form'=>$form]);
            }
            $data = array_merge_recursive($data,$file);
            $newName = date('Y-m-d-h-i-s').'-'.$file['image']['name'];

            $image = new Rename([
                'target'=>IMAGE_PATH.'/hinh_mon_an/'.$newName,
                'overwrite'=>true
            ]);
    
            $image->filter($file['image']);
            $data['image'] = $newName;
        }else{
            $data['image'] = $form->get('image')->getValue();
           
        }
        
        $form->setData($data);
            
        if(!$form->isValid()){
            return new ViewModel(['form'=>$form]);
        }
        $data = $form->getData(); //$data dạng object
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // die;
        $data->update_at = date('Y-m-d');
        //$data['promotion'] = implode($data['promotion'],', '); //Đưa giá trị dạng mảng thành dạng chuỗi

        $this->table->saveFoods($data);
        $this->flashMessenger()->addSuccessMessage('Cập nhật thành công!');
        return $this->redirect()->toRoute('foods',['controller'=>'FoodsController','action'=>'index']);
    }

    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id',0);
        if(!$id){
            return $this->redirect()->toRoute('foods');
        }
        $checkRequest = $this->getRequest();
        if($checkRequest->isPost()){
            $del  = $checkRequest->getPost('del','No');
            if($del == 'Yes'){
                $id = (int)$checkRequest->getPost('id');
                $this->table->deleteFood($id);
                $this->flashMessenger()->addSuccessMessage('Xóa thành công');
            }
            return $this->redirect()->toRoute('foods');
        }
        return [
            'id'=>$id,
            'foods'=>$this->table->findFoods($id)
        ];
    }
}